﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Entites
{
    public class Line
    {
        private Color _color = Color.Black;
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private float _width = 1;
        public float Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// 指圆心到圆心的距离
        /// </summary>
        public float Length { get; set; }

        public Enums.LineType LineType { get; set; }
    }
}
