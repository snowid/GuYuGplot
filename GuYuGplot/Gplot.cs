﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GuYuGplot.Entites;
using GuYuGplot.Drawing;

namespace GuYuGplot
{
    public partial class Gplot : UserControl
    {
        private DrawingOperator _drawingOperator = new DrawingOperator();//绘制的逻辑在该类中实现

        public Gplot()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.MouseDoubleClick += new MouseEventHandler(Gplot_MouseDoubleClick);
            this.MouseClick += new MouseEventHandler(Gplot_MouseClick);
        }

        private NodesTree _nodesTree = new NodesTree();

        /// <summary>
        /// 节点树形结构
        /// </summary>
        public NodesTree NodesTree
        {
            get { return _nodesTree; }
            set { _nodesTree = value; }
        }

        /// <summary>
        /// 双击节点事件
        /// </summary>
        public event EventHandler<NodeClickEventArgs> NodeDoubleClick;

        /// <summary>
        /// 单击节点事件
        /// </summary>
        public event EventHandler<NodeClickEventArgs> NodeClick;

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            _drawingOperator.Draw(this.NodesTree, graphics, this.Width, this.Height);
        }

        private void Gplot_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void Gplot_MouseClick(object sender, MouseEventArgs e)
        {
            String nodeName;
            if (this.NodeClick != null)
            {
                if (_drawingOperator.InNode(e.X, e.Y, out nodeName))
                {
                    NodeClickEventArgs args = new NodeClickEventArgs();
                    args.NodeName = nodeName;
                    this.NodeClick(sender, args);
                }
            }
        }

        private void Gplot_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            String nodeName;
            if (this.NodeDoubleClick != null)
            {
                if (_drawingOperator.InNode(e.X, e.Y, out nodeName))
                {
                    NodeClickEventArgs args = new NodeClickEventArgs();
                    args.NodeName = nodeName;
                    this.NodeDoubleClick(sender, args);
                }
            }
        }
    }

    public class NodeClickEventArgs : EventArgs
    {
        public String NodeName { get; set; }
    }
}
